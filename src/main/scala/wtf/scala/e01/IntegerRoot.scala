package wtf.scala.e01

object IntegerRoot {

  /**
    * Assume x is non negative integer.
    * If x = y * y for some integer y, then return y, otherwise return -1
    * @param x non negative integer to calculate square root
    * @throws IllegalArgumentException if negative number passed (Hint: check require method)
    * @return -1 if x is not square of some integer
    *         square root of x if x is square of some integer
    */
  @throws[IllegalArgumentException]
  def calculateRoot(x: Int): Int = {
    def root(y: Int, dy: Int): Int = {
      val yy = y*y;

      if      (x == yy) y
      else if (dy == 0) -1
      else if (x < yy)  root(y - dy, dy/2)
      else              root(y + dy, dy/2)
    }
    require(x >= 0)
    root(x/2, x/4)
  }
}
