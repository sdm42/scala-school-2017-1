package wtf.scala.e01

object FizzBuzz {

  /**
    * Return "fizz", "buzz", "fizzbuzz" or number between 0 (inclusive) and n (exclusive).
    * For a given natural number greater than zero return:
    *   "fizzbuzz" if the number is dividable by 3 and 5
    *   "buzz" if the number is dividable by 5
    *   "fizz" if the number is dividable by 3
    * the same number if no other requirement is fulfilled
    * @param n
    * @throws IllegalArgumentException if n < 1
    * @return seq of fizzbuzz strings
    */
  @throws[IllegalArgumentException]
  private[e01] def fizzBuzzUntil(n: Int): Seq[String] = {
    if (n < 1) throw new IllegalArgumentException()
    else {
      for {
        i <- 0 until n
      } yield (by3(i) + by5(i)) match {
        case 0 => i.toString
        case 1 => "fizz"
        case 2 => "buzz"
        case _ => "fizzbuzz"
      }
    }
  }
 
  private def by3(n: Int): Int = if (0 == n % 3)  1 else 0
  private def by5(n: Int): Int = if (0 == n % 5)  2 else 0


  def main(args: Array[String]): Unit = {
    println(fizzBuzzUntil(100).mkString(" "))
  }
}
