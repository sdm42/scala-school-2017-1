package wtf.scala.e03

object Fibonacci {

  /**
    * Compute the n th Fibonacci number using recursion
    * https://en.wikipedia.org/wiki/Fibonacci_number
    *
    * We assume that fib(1) = 0, fib(2) = 1, and so on
    * @param n
    * @return
    */
  def fibRecursive(n: Int): Long = {
    def fib(prev: Long, cur: Long, i: Int): Long = {
      if (i == 0) cur
      else fib(cur, prev + cur, i - 1)
    }
    require(n > 0)
    if (n == 1) 0
    else        fib(0, 1, n - 2)
  }

  /**
    * Compute the n th Fibonacci number using memoization
    * https://en.wikipedia.org/wiki/Fibonacci_number
    *
    * We assume that fib(1) = 0, fib(2) = 1, and so on
    *
    * Hint: to store computed Fibonacci numbers you can use scala.collection.mutable.HashMap[Int, Long]
    * @param n
    * @return
    */
  private val mem = scala.collection.mutable.HashMap[Int, Long](1 -> 0, 2 -> 1)
  def fibMemo(n: Int): Long = {
    def findFirst(i: Int): Int = if (mem.contains(i)) i else findFirst(i - 1)
    for {
      i <- findFirst(n) until n
    } mem(i + 1) = mem(i) + mem(i - 1)

    mem(n)
  }
}
