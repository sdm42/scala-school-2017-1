package wtf.scala.e03

object FactorialIteration {

  /**
    * As we know, tail recursive function can be optimized into iteration
    * Here you need to create iterational version of factorial yourself
    * @param n - number to compute factorial for
    * @return
    */
  def factorial(n: Long): Long = {
    require(n >= 0)
    var i = n;
    var acc: Long = 1;
    while (i != 0) {
      acc *= i
      i -= 1
    }
    acc
  }

}
